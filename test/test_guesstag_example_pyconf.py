"""
Unit tests for guesstag.py
"""

from guesstag import guesstag

import unittest


class GuessTagExamplePyconfTest(unittest.TestCase):
    """Unit tests the guess tag module and the `example_pyconf.yml` file"""

    # -------------------------------------------------------------------------
    def test_process_file(self):
        """Can we deal with an example file?
        """
        version = guesstag._try_to_set_debut_version('test/example_pyconf.yml')

        self.assertEqual(version, [2018, 8])


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
