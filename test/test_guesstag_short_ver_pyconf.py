"""
Unit tests for guesstag.py
"""

from guesstag import guesstag

import unittest


class GuessTagExamplePyconfTest(unittest.TestCase):
    """Unit tests the guess tag module and the `example_pyconf.yml` file"""

    # -------------------------------------------------------------------------
    def test_process_file(self):
        """Can we deal with a missing file?
        """
        version = guesstag._try_to_set_debut_version('test/short_ver_pyconf.yml')

        self.assertEqual(version, [0, 0, 0])


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
