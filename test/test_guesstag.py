"""
Unit tests for guesstag.py
"""

from guesstag import guesstag

from io import StringIO
from subprocess import CompletedProcess
import sys
import unittest
from unittest.mock import MagicMock, patch


@patch('subprocess.run')
class GuessTagTest(unittest.TestCase):
    """Unit tests the basic guess tag module"""

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_1_2_3(self, mock_run):
        """Can we decompose '1.2.3'?
        """
        version = guesstag._try_to_decompose_version('1.2.3')

        self.assertEqual(version, [1, 2, 3])

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_1_2_3a4(self, mock_run):
        """Can we decompose '1.2.3a4'?
        """
        version = guesstag._try_to_decompose_version('1.2.3a4')

        self.assertEqual(version, [1, 2, 3])

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_1_2_3_a4(self, mock_run):
        """Can we decompose '1.2.3-a4'?
        """
        version = guesstag._try_to_decompose_version('1.2.3-a4')

        self.assertEqual(version, [1, 2, 3])

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_17_02(self, mock_run):
        """Can we decompose '17.02'?
        """
        version = guesstag._try_to_decompose_version('17.02')

        self.assertEqual(version, [17, 2])

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_17_02_a2(self, mock_run):
        """Can we decompose '17.02-a2'?
        """
        version = guesstag._try_to_decompose_version('17.02-a2')

        self.assertEqual(version, [17, 2])

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_1(self, mock_run):
        """Can we decompose '1'?
        """
        version = guesstag._try_to_decompose_version('1')

        self.assertIsNone(version)

    # -------------------------------------------------------------------------
    def test_try_to_decompose_version_abc(self, mock_run):
        """Can we decompose 'a.b.c'?
        """
        version = guesstag._try_to_decompose_version('a.b.c')

        self.assertIsNone(version)

    # -------------------------------------------------------------------------
    def test_001(self, mock_run):
        """Given that the current tag is 0.0.1,
        assert that guesstag returns 0.0.2
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'0.0.1'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.2')

    # -------------------------------------------------------------------------
    def test_main(self, mock_run):
        """Given that the current tag is 0.0.1,
        assert that guesstag returns 0.0.2 (via the main method).

        This should 'print' the result and so we need to capture
        the stdout stream.
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'0.0.1'
        mock_run.return_value = mock_cp
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out

            guesstag.main()

            output = out.getvalue().strip()
            self.assertEqual(output, '0.0.2')
        finally:
            sys.stdout = saved_stdout

    # -------------------------------------------------------------------------
    def test_001_with_spaces(self, mock_run):
        """Given that the current tag is '0. 0 .1',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'0. 0 .1'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_abc(self, mock_run):
        """Given that the current tag is a.b.c,
        assert that guesstag returns 0.0.2
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'a.b.c'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_blank(self, mock_run):
        """Given that the current tag is empty,
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b''
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_no_minor(self, mock_run):
        """Given that the current tag is '1',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_non_number_year(self, mock_run):
        """Given that the current tag is 'a.2',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'a.2'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_non_number_month(self, mock_run):
        """Given that the current tag is '2018.a',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2018.a'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_year_version(self, mock_run):
        """Given that the current tag is '2018.8',
        assert that guesstag returns 2017.9
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2018.8'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '2018.9')

    # -------------------------------------------------------------------------
    def test_year_version_with_snapshot_suffix(self, mock_run):
        """Given that the current tag is '2018.8-SNAPSHOT',
        assert that guesstag returns 2018.9
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2018.8-SNAPSHOT'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '2018.9')

    # -------------------------------------------------------------------------
    def test_year_version_with_python_alpha_suffix(self, mock_run):
        """Given that the current tag is '2018.8a4',
        assert that guesstag returns 2018.9
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2018.8a4'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '2018.9')

    # -------------------------------------------------------------------------
    def test_year_version_with_big_month(self, mock_run):
        """The 2nd part of 2-digits is not a month.
        check large numbers work.
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2018.800'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '2018.801')

    # -------------------------------------------------------------------------
    def test_non_number_major(self, mock_run):
        """Given that the current tag is 'a.2.3',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'a.2.3'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_non_number_minor(self, mock_run):
        """Given that the current tag is '1.b.3',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.b.3'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_non_number_patch(self, mock_run):
        """Given that the current tag is '1.2.c',
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.2.c'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '0.0.0')

    # -------------------------------------------------------------------------
    def test_yymm(self, mock_run):
        """Given that the current tag is '17.4',
        assert that guesstag returns 17.5
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'17.4'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '17.5')

    # -------------------------------------------------------------------------
    def test_alpha(self, mock_run):
        """Given that the current tag is '1.2.0a10,
        assert that guesstag returns 1.2.1
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.2.0a10'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '1.2.1')

    # -------------------------------------------------------------------------
    def test_beta(self, mock_run):
        """Given that the current tag is '1.2.0b10,
        assert that guesstag returns 1.2.1
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.2.0b10'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '1.2.1')

    # -------------------------------------------------------------------------
    def test_candidate(self, mock_run):
        """Given that the current tag is '1.2.0c10,
        assert that guesstag returns 1.2.1
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.2.0c10'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '1.2.1')

    # -------------------------------------------------------------------------
    def test_hyphen(self, mock_run):
        """Given that the current tag is '1.2.0-a10,
        assert that guesstag returns 0.0.0
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'1.2.0-a10'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '1.2.1')

    # -------------------------------------------------------------------------
    @patch.dict('os.environ', {'CI_COMMIT_TAG': '4.5.10'})
    def test_ci_commit_tag(self, mock_run):
        """Given that the current tag is '2.4.5',
        assert that guesstag returns 4.5.10
        (because it's what it defined in CI_COMMIT_TAG)
        """
        mock_cp = MagicMock(CompletedProcess)
        mock_cp.stdout = b'2.4.5'
        mock_run.return_value = mock_cp

        next_tag = guesstag._guess_tag()

        self.assertEqual(next_tag, '4.5.10')


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
