#!/usr/bin/env python3

# Setup module for the Python-based GuessTag Tool
#
# Aug 2018

import os
from setuptools import setup, find_packages


# Use the README.rst as the long description.
def get_long_description():
    return open('README.rst').read()


# Pull in the essential run-time requirements
# We use an external file so we can do a
# `pip install -r runtime-build-requirements.txt`
# when building a Docker image.
with open('requirements.txt') as file:
    requirements = file.read().splitlines()

name = 'matildapeak-guesstag'
author = 'Alan Christie'
version = os.environ.get('CI_COMMIT_TAG', '2018.1')
copyright = 'MIT'
setup(

    name=name,
    version=version,
    author=author,
    author_email='alan.christie@matildapeak.com',
    url='https://gitlab.com/matilda.peak/guesstag',
    license=copyright,
    description='Simple configuration tool',
    long_description=get_long_description(),
    keywords='configuration',
    platforms=['any'],
    # These are optional and override the Sphinx conf.py settings
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'copyright': ('setup.py', author),
            'source_dir': ('setup.py', 'doc')
        }
    },

    # Our modules to package
    py_modules=['__main__', 'guesstag'],
    packages=find_packages(exclude=["*.test", "*.test.*", "test.*", "test"]),

    # Project classification:
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: System :: Installation/Setup',
        'Operating System :: POSIX :: Linux',
    ],

    install_requires=requirements,

    entry_points={
        "console_scripts": [
            "guesstag = guesstag.guesstag:main",
        ],
    },

    zip_safe=False,

)
